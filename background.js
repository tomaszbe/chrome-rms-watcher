var senderTabs = {}
var senderTargetUrls = {}

function notify (title, message, tab, callback) {
	var time = new Date()
	var minutes = time.getMinutes()
	chrome.notifications.create({
		type: 'basic',
		iconUrl: 'meh.svg',
		title: title,
		message: message,
		contextMessage: time.getHours() + ':' + (minutes < 10 ? '0' : '') + minutes,
		buttons: [
			{ title: 'Otwórz', iconUrl: 'check.svg' },
			{ title: 'Pokaż zakładkę', iconUrl: 'columns.svg' }
		],
		isClickable: true,
		requireInteraction: true
	}, callback)
}

function clearNotification (id) {
	delete senderTabs[id]
	delete senderTargetUrls[id]
	chrome.notifications.clear(id)
}
function focusTab(tab) {
	chrome.tabs.update(tab.id, {active: true, highlighted: true})
	chrome.windows.update(tab.windowId, {focused: true})
}

function openItem(notificationId) {
	chrome.tabs.create({
		url: senderTargetUrls[notificationId],
		windowId: senderTabs[notificationId].windowId,
	})
}

function saxButton(notificationId) {
	chrome.notifications.update(notificationId, {
		buttons: [
			{ title: 'Otwórz', iconUrl: 'check.svg' },
			{ title: 'A co to?', iconUrl: 'magic.svg' }
		]
	})
}

function openSax() {
	chrome.tabs.create({
		url: 'https://www.youtube.com/watch?v=G1IbRujko-A',
		pinned: true
	})
}

chrome.runtime.onMessage.addListener(function (data, sender) {
	notify(data.title, data.message, sender.tab, function (id) {
		senderTabs[id] = sender.tab
		senderTargetUrls[id] = data.targetUrl
		if (id[0] === 'a') {
			saxButton(id)
		}
	})
})

chrome.notifications.onButtonClicked.addListener(function (id, button) {
	if (button === 0) {
		openItem(id)
	} else {
		if (id[0] === 'a') {
			openSax()
		} else {
			focusTab(senderTabs[id])
		}
	}
	clearNotification(id)
})

chrome.notifications.onClicked.addListener(function (id) {
	focusTab(senderTabs[id])
	clearNotification(id)
})