var button
var observing = false
var interval
var audio
var lastCount

function getFilters() {
  var inputs = document.querySelectorAll('.filters .k-input')
  var textBoxes = document.querySelectorAll('.filters .k-textbox')
  var filters = []
  inputs.forEach(function(item) {
    filters.push(item.innerText)
  })
  textBoxes.forEach(function(item) {
    filters.push(item.value)
  })

  filters = filters.filter(function(text) {
    return text != '' && text.toLowerCase() != 'wybierz'
  })
  return filters.join(', ')
}

function getTitle() {
  return document.title.replace('RMS', 'nowy element')
}

function getItemLink(id) {
  return document.location.href + '/Edit/' + id
}

function notify(id) {
  // audio.play()
  chrome.runtime.sendMessage({
    title: getTitle(),
    message: getFilters(),
    targetUrl: getItemLink(id)
  })
}

function initAudio() {
  audio = new Audio('http://www.soundjay.com/button/beep-07.wav')
  audio.volume = 0.1
}

function injectScript(file_path, tag) {
  var node = document.getElementsByTagName(tag)[0]
  var script = document.createElement('script')
  script.setAttribute('type', 'text/javascript')
  script.setAttribute('src', file_path)
  node.appendChild(script)
}

function setButtonText() {
  button.value = observing ? 'Obserwuję...' : 'Obserwuj'
}

function insertButton() {
  var div = document.getElementsByClassName('search')[0]
  if (!div) {
    return
  }
  button = document.createElement('input')
  button.type = 'button'
  button.id = 'rms-button'
  button.classList.value =
    'k-button k-button-icontext k-add-button actionbutton'
  setButtonText()
  button.addEventListener('click', toggle)
  div.appendChild(button)
}

function refresh() {
  document.dispatchEvent(new Event('rms-refresh'))
}

function toggle() {
  observing = !observing
  if (observing) {
    document.dispatchEvent(new Event('rms-start'))
    interval = setInterval(refresh, 60000)
  } else {
    document.dispatchEvent(new Event('rms-stop'))
    clearInterval(interval)
  }
  setButtonText()
}

// const unreads = []

function markAsUnread(uid) {
  document
    .querySelector(`tr[data-uid="${uid}"]`)
    .classList.add('hasAlert', 'lvl4')
}

// function markAsRead(uid) {
//   document
//     .querySelector(`tr[data-uid="${uid}"]`)
//     .classList.remove('hasAlert', 'lvl4')
// }

function handleNewItem(e) {
  const { id, uid } = e.detail
  notify(id)
  markAsUnread(uid)
}

;(function() {
  insertButton()
  if (!button) {
    return
  }
  // initAudio()
  injectScript(chrome.extension.getURL('content.js'), 'body')
  document.addEventListener('rms-new-item', handleNewItem)
})()
