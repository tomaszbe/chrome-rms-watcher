;(function() {
  let isWatching = false
  let items = []

  const getCurrentItems = () => window.dataSource.data()
  const refresh = () => window.dataSource.read()
  const start = () => {
    items = getCurrentItems()
    isWatching = true
  }
  const stop = () => {
    isWatching = false
  }

  const compareItems = (current, old) => {
    const newItems = current.filter(item => {
      return !old.find(oldItem => oldItem.Id === item.Id)
    })

    return newItems
  }

  const onAjaxComplete = () => {
    if (!isWatching) {
      return
    }
    const currentItems = getCurrentItems()
    const newItems = compareItems(currentItems, items)
    if (newItems.length === 0) {
      return
    }
    items = currentItems
    newItems.forEach(notifyAboutNewItem)
  }

  /* Dispatch events */
  const notifyAboutNewItem = item => {
    document.dispatchEvent(
      new CustomEvent('rms-new-item', {
        detail: { id: item.Id, uid: item.uid }
      })
    )
  }

  /* Attach event listeners */
  $(document).ajaxComplete(onAjaxComplete)
  document.addEventListener('rms-start', start)
  document.addEventListener('rms-stop', stop)
  document.addEventListener('rms-refresh', refresh)
})()
